const express = require('express');
const router = express.Router();
const db =require('../models/index')

const app = express();

// Static File
app.use(express.static('public'))
app.use('/css', express.static(__dirname + '/public/assets/css'))
app.use('/js', express.static(__dirname + '/public/js'))
app.use('/img', express.static(__dirname + '/public/assets/img'))

let user_id=[];
router.get('/',(req,res)=>{
   console.log(user_id)
    db.User.findAll()
    .then((user)=>{
        res.status(200).render('users/dashboard',{user_id,user});
    })
    .catch((err)=>{
        res.status(400).json({
            message:err.message
        })
    })
})


router.get('/signup',(req,res)=>{
    res.status(200).render('users/siginup')
 })
 

router.get('/signin',(req,res)=>{
    res.status(200).render('users/signin')
 })

router.get('/:id',(req,res)=>{
    db.User.findByPk(req.params.id)
    .then((user)=>{
        res.status(200).render('users/edit',{user});
    })
    .catch((err)=>{
        res.status(400).json({
            message:err.message
        })
    })
})

router.post('/',(req,res)=>{
    const {username,email,password} = req.body;
    db.User.create({username,email,password})
    .then((user)=>{
        res.status(200).redirect('/users/signin')
    })
    .catch((err)=>{
        res.status(400).json({
            message:err.message
        })
    })
})
router.put('/:id',(req,res)=>{
    const {username,password} = req.body;
    db.User.update(req.body,{
        where:{
            id:req.params.id
        }
    }).then((user)=>{
        res.status(201).redirect('/users')
    }).catch((err)=>{
        res.status(400).json({
            message:err.message
        })
    })
})

router.delete('/:id',(req,res)=>{
    db.User.destroy({
        where:{
            id:req.params.id
        }
    }).then(user =>{
        res.status(201).redirect('/users')
    }).catch(err=>{
        res.status(400).json({
            message:err.message
        })
    })
})

router.post('/login',(req,res)=>{
    const {username,password} = req.body;
    db.User.findOne({
        where:{
            username:username,
            password:password
        }
    })
    .then((user)=>{
        user_id = user;
        if(user.username === username){
            res.status(200).redirect('/users')
        }
    })
    .catch((err)=>{
        res.status(400).json({
            message:err.message
        })
    })
})


router.post('/play',(req,res)=>{
    const {username,result} = req.body;
    db.User_History.create({username,result })
    .then((user)=>{
        user_id = user;
        res.status(200).redirect('/users')
    })
    .catch((err)=>{
        res.status(400).json({
            message:err.message
        })
    })
})





module.exports = router;
