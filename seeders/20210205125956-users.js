'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {

 return queryInterface.bulkInsert('Users', [
   {
        name: 'John Doe',
        username:'john',
        email: 'john@contoh.com',
        password:'1234',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'The rock',
        username:'rock',
        email: 'rock@contoh.com',
        password:'1234',
        createdAt: new Date(),
        updatedAt: new Date(),
      },  

    ]);
    
  },

  down: async (queryInterface, Sequelize) => {
  
      await queryInterface.bulkDelete('Users', null, {
        truncate:true,
        restartIdentity:true
      });
  }
};
