const express = require('express');
const morgan =require('morgan');
const app = express();
const db = require('./models/index')
const methodOverride = require('method-override');
const PORT = process.env.PORT || 3000;

app.use(methodOverride('_method'))
app.use(morgan('dev'));
app.use(express.json());
app.use(express.urlencoded({extended:false}));

// Set view
app.set('view engine' ,'ejs')

// Static File
app.use(express.static('public'))
app.use('/css', express.static(__dirname + '/public/assets/css'))
app.use('/js', express.static(__dirname + '/public/js'))
app.use('/img', express.static(__dirname + '/public/assets/img'))
app.use('/fonts', express.static(__dirname + '/public/assets/fonts'))
app.use('/images', express.static(__dirname + '/public/assets/images'))
app.use('/vendor', express.static(__dirname + '/public/assets/vendor'))


// Routes
const indexRoutes =require('./routes/index.route')
const userRoutes = require('./routes/user.route')



app.use('/', indexRoutes);
app.use('/users/',userRoutes);












app.use((req,res,next)=>{
    const error = new Error('NOT FOUND');
    error.status= 404;
    next(error);
})

app.use((error, req,res,next)=>{
    res.status(error.status || 500);
    res.json({
        error:{
            message:error.message,
        }
    })
})

app.listen(PORT, ()=>{
    console.log(`listening port ${PORT}`);
})